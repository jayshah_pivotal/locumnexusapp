package com.nexus.locum.locumnexus;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nexus.locum.locumnexus.utilities.Const;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nexus.locum.locumnexus.fragments.SignupStepOne.arrayList_prefixName;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.spinnerArrayAdapter;
import static com.nexus.locum.locumnexus.utilities.APICall.post;
import static com.nexus.locum.locumnexus.utilities.Const.CONST_SHAREDPREFERENCES;
import static com.nexus.locum.locumnexus.utilities.Const.MyPREFERENCES;
import static com.nexus.locum.locumnexus.utilities.Const.PREF_USER_TOKEN;

public class LoginLocum extends AppCompatActivity {

    TextInputLayout TIL_emailid ,TIL_password;
    EditText et_email ,et_password;
    TextView tv_register,tv_forgotPassword;
    Button btn_sigin;
    String str_email,str_password;

    final Context context = this;


    private static final String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,40})";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_locum);

        Const.CONST_SHAREDPREFERENCES  = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);



        TIL_emailid = (TextInputLayout)this.findViewById(R.id.TIL_emailid);
        TIL_password = (TextInputLayout)this.findViewById(R.id.TIL_password);

        et_email = (EditText) this.findViewById(R.id.et_email);
        final String email = et_email.getEditableText().toString().trim();
        final String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";


        et_password = (EditText) this.findViewById(R.id.et_password);

        tv_register = (TextView) this.findViewById(R.id.tv_register);
        tv_register.setText(Html.fromHtml("<font color=\"#004670\">" + "Register   as   " + "</font>" + "<u><font color=\"#00A75B\" >Locum" + "</font></u>"));
        tv_forgotPassword = (TextView)this.findViewById(R.id.tv_forgotPassword);

        btn_sigin = (Button) this.findViewById(R.id.btn_sigin);
        btn_sigin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(et_email.getText().length()==0 || et_password.getText().length()==0){
                    if(et_email.getText().length()==0){
                        et_email.setError("Enter Email !");
                    }else{
                        et_password.setError("Enter Password !");
                    }
                }else if(!isEmailValid(et_email.getText().toString())){
                    et_email.setError("Enter Valid Email !");
                }/*else if(!isPasswordValid(et_password.getText().toString())){
                    et_password.setError("Password must be 8-15 character\nhas 1 cap&small alphabet\nhas number\nhas special character.");
                }*/else{
                    str_email= et_email.getText().toString();
                    str_password=et_password.getText().toString();
                    new LoginLocumAPI().execute();

                    /*startActivity(new Intent(LoginLocum.this,SignupLocum.class));
                    finish();*/
                }
            }
        });

        tv_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginLocum.this,SignupLocum.class));
                //finish();
            }
        });

        tv_forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // get prompts.xml view
                LayoutInflater li = LayoutInflater.from(context);
                View promptsView = li.inflate(R.layout.dialog_forgot_password, null);

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

                // set prompts.xml to alertdialog builder
                alertDialogBuilder.setView(promptsView);

                final EditText userInputEmal = (EditText) promptsView
                        .findViewById(R.id.et_forgot_password_email);

                // set dialog message
                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        // get user input and set it to result
                                        // edit text
                                        if(!isEmailValid(userInputEmal.getText().toString())){
                                            userInputEmal.setError("Enter Valid Email !");
                                        }else{
                                            dialog.dismiss();
                                        }
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });

                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();

                // show it
                alertDialog.show();



            }
        });


    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public boolean isPasswordValid(String password) {
        boolean isValid = false;
        String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,15})";

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);

        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    String resUserDetails;
    protected ProgressDialog progressDialog;
    private class LoginLocumAPI extends AsyncTask<Object, Void, String> {

        @Override
        protected void onPreExecute()//execute thaya pela
        {
            super.onPreExecute();
            // Log.d("pre execute", "Executando onPreExecute ingredients");
            //inicia diálogo de progress, mostranto processamento com servidor.
            progressDialog = ProgressDialog.show(LoginLocum.this, "Loading", "Please Wait...", true, false);

        }

        @Override
        protected String doInBackground(Object... parametros) {

            try {

                JsonObject LoginJson = new JsonObject();
                LoginJson.addProperty("email",str_email);
                LoginJson.addProperty("password",str_password);
                //Log.d("data send--",""+LoginJson.toString());

                String responseUSerTitles = post(Const.SERVER_URL_ONLY +"auth/local", LoginJson.toString(),"post");
                // Log.d("URL ====",Const.SERVER_URL_API+"filter_venues?"+upend);
                resUserDetails=responseUSerTitles;


            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resUserDetails;
        }


        @Override
        protected void onPostExecute(String result) {

            String response_string = "";
            // System.out.println("OnpostExecute----done-------");
            super.onPostExecute(result);


            try{
                Log.i("RES UserDetails---", resUserDetails);
                JsonParser parser = new JsonParser();
                JsonObject rootObj = parser.parse(resUserDetails).getAsJsonObject();

                if(rootObj.has("error")){

                }else{
                    String token = rootObj.get("token").getAsString();
                    CONST_SHAREDPREFERENCES.edit().putString(PREF_USER_TOKEN,token).apply();
                }
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            progressDialog.dismiss();
        }
    }


}
