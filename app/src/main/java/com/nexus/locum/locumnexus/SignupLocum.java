package com.nexus.locum.locumnexus;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.nexus.locum.locumnexus.fragments.SignupStepOne;
import com.nexus.locum.locumnexus.fragments.SignupStepTwo;
import com.nexus.locum.locumnexus.utilities.Const;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.nexus.locum.locumnexus.fragments.SignupStepOne.arrayList_prefixName;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.et_cnfemail;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.et_email;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.et_firstName;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.et_lastName;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.et_mobileno;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.rg_gender;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.sp_prefixName;
import static com.nexus.locum.locumnexus.fragments.SignupStepOne.spinnerArrayAdapter;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.TIL_catTypeNumber;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.et_catTypeNumber;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.et_confirmPassword;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.et_password;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.isTermsAccepted;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.rg_plan;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.rg_plantype;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.sp_category;
import static com.nexus.locum.locumnexus.fragments.SignupStepTwo.tv_termscondition;
import static com.nexus.locum.locumnexus.utilities.APICall.post;
import static com.nexus.locum.locumnexus.utilities.Const.CONST_SHAREDPREFERENCES;
import static com.nexus.locum.locumnexus.utilities.Const.MyPREFERENCES;
import static com.nexus.locum.locumnexus.utilities.Const.PREF_USER_TOKEN;

public class SignupLocum extends AppCompatActivity {

    JsonObject SignupJsonRequest = new JsonObject();
    JsonArray rootArrayCategory;

    protected ProgressDialog progressDialog;

    public static ArrayList<String> arrayList_category = new ArrayList<String>();
    public static ArrayAdapter<String> spinnerCategoryAdapter;


    Button firstFragment, secondFragment,btn_submit;
    LinearLayout ll_stepssignup;
    TextView tv_alreadyaccount;
    int step_no=1;
    public static String str_prifixname,str_firstName,str_lastName,str_gender,str_email,str_mobile,
            str_plan,str_plan_type,str_category,str_categoryNumber,str_TitlecatTypeNumber,str_password;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_locum);

        Const.CONST_SHAREDPREFERENCES  = getApplicationContext().getSharedPreferences(MyPREFERENCES, MODE_PRIVATE);


        spinnerCategoryAdapter = new ArrayAdapter<String>(getBaseContext(),R.layout.onlytextview,arrayList_category);
        spinnerCategoryAdapter.setDropDownViewResource(R.layout.onlytextview);


        new GetUeserTitleGetCategory().execute();


        btn_submit = (Button) findViewById(R.id.btn_submit);

        ll_stepssignup = (LinearLayout)this.findViewById(R.id.ll_stepssignup);

        tv_alreadyaccount = (TextView) findViewById(R.id.tv_alreadyaccount);
        tv_alreadyaccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(SignupLocum.this,LoginLocum.class));
                finish();
            }
        });

        // get the reference of Button's
        firstFragment = (Button) findViewById(R.id.firstFragment);
        secondFragment = (Button) findViewById(R.id.secondFragment);

        loadFragment(new SignupStepOne());


        // perform setOnClickListener event on First Button
        firstFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // load First Fragment
                //loadFragment(new SignupStepOne());
            }
        });

        // perform setOnClickListener event on Second Button
        secondFragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // load Second Fragment
                //loadFragment(new SignupStepTwo());
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(step_no==1) {
                    /*if(et_firstName.getText().length()<=0 || et_lastName.getText().length()<=0 ||
                            et_email.getText().length()<=0 || et_cnfemail.getText().length()<=0 ||
                            et_mobileno.getText().length()<=0){
                        if(et_firstName.getText().length()<=0){
                            et_firstName.setError("Required !");
                        }else if(et_lastName.getText().length()<=0){
                            et_lastName.setError("Required ! ");
                        }else if(et_email.getText().length()<=0){
                            et_email.setError("Required ! ");
                        }else if(et_cnfemail.getText().length()<=0){
                            et_cnfemail.setError("Required ! ");
                        }else if(et_mobileno.getText().length()<=0){
                            et_mobileno.setError("Required ! ");
                        }

                    }else if(!isEmailValid(et_email.getText().toString())){
                        et_email.setError("Enter Valid Email Id.");
                    }else if(!et_email.getText().toString().equals(et_cnfemail.getText().toString())){
                        et_cnfemail.setError("Email Not Matched .");
                    }else if (et_mobileno.getText().length()<10){
                        et_mobileno.setError("Enter 10 Digit Mobile Number.");
                    }else */if (rg_gender.getCheckedRadioButtonId() == -1)
                    {// no radio buttons are checked
                        Toast.makeText(getBaseContext(),"Please Select Gender .",Toast.LENGTH_LONG).show();
                    }else {
                        loadFragment(new SignupStepTwo());
                        step_no = 2;
                        btn_submit.setText("Sign Up");
                        ll_stepssignup.setBackgroundResource(R.drawable.steptwo);

                        int selectedId=rg_gender.getCheckedRadioButtonId();
                        RadioButton radioGenderButton=(RadioButton)findViewById(selectedId);
//                        Toast.makeText(getBaseContext(),radioGenderButton.getText(),Toast.LENGTH_SHORT).show();

                        str_prifixname = sp_prefixName.getSelectedItem().toString();
                        str_firstName = et_firstName.getText().toString();
                        str_lastName = et_lastName.getText().toString();
                        str_email = et_email.getText().toString();
                        str_mobile = et_mobileno.getText().toString();
                        str_gender = radioGenderButton.getText().toString();

                    }



                    //firstFragment.setBackgroundColor(getResources().getColor(R.color.colorPrimaryDark));
                    //secondFragment.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                }else if(step_no==2){

                    if(et_password.getText().length()<=0 || et_confirmPassword.getText().length()<=0){

                        if(et_password.getText().length()<=0){
                            et_password.setError("Required !");
                        }else if(et_confirmPassword.getText().length()<=0){
                            et_confirmPassword.setError("Required ! ");
                        }

                    }else if(sp_category.getSelectedItemPosition()==0){
                        et_email.setError("Enter Valid Email Id.");
                        ((TextView)sp_category.getSelectedView()).setError("Select Category");
                    }else if(!isPasswordValid(et_password.getText().toString())){
                        et_password.setError("Password must be 8-15 character\nhas 1 cap&small alphabet\nhas number\nhas special character.");
                    }else if(!et_password.getText().toString().equals(et_confirmPassword.getText().toString())){
                        et_confirmPassword.setError("Password Not Matched .");
                    }else if (et_catTypeNumber.getText().length()<=0){
                        et_catTypeNumber.setError("Required !");
                    }else if(TIL_catTypeNumber.getHint().toString().equalsIgnoreCase("NMC Number")
                            &&  et_catTypeNumber.getText().length()!=8){
                        et_catTypeNumber.setError("Enter Valid Number !");
                    }else if((TIL_catTypeNumber.getHint().toString().equalsIgnoreCase("GDC Number")
                            || TIL_catTypeNumber.getHint().toString().equalsIgnoreCase("GMC Number"))
                            &&  et_catTypeNumber.getText().length()!=7){
                        et_catTypeNumber.setError("Enter Valid Number !");
                    }else if(isTermsAccepted!=true){
                        tv_termscondition.setError("Please Read and Accept !");
                    } else {



                        int selectedIdPlan=rg_plan.getCheckedRadioButtonId();
                        RadioButton radioButtonPlan=(RadioButton)findViewById(selectedIdPlan);
                       // Toast.makeText(getBaseContext(),radioButtonPlan.getText(),Toast.LENGTH_SHORT).show();

                        int selectedIdPlanType=rg_plantype.getCheckedRadioButtonId();
                        RadioButton radioButtonPlanType=(RadioButton)findViewById(selectedIdPlanType);
                        //Toast.makeText(getBaseContext(),radioButtonPlanType.getText(),Toast.LENGTH_SHORT).show();

                        str_password = et_password.getText().toString();
                        str_category = sp_category.getSelectedItem().toString();

                        Log.e("Selectd Category--",str_category);

                        JsonObject CategoryRequestJson = null;
                        for(int a=0;a<rootArrayCategory .size();a++){
                            if(str_category.equalsIgnoreCase(rootArrayCategory.get(a).getAsJsonObject().get("name").getAsString())){
                                CategoryRequestJson = rootArrayCategory.get(a).getAsJsonObject();

                                //Log.e("got CatfromArray--",rootArrayCategory.get(a).getAsJsonObject().get("name").getAsString());
                            }
                        }

                        str_categoryNumber = et_catTypeNumber.getText().toString();
                        str_TitlecatTypeNumber = TIL_catTypeNumber.getHint().toString();


                        str_plan = radioButtonPlan.getText().toString();
                        str_plan_type = radioButtonPlanType.getText().toString();
                        str_plan_type = str_plan_type.substring(2,str_plan_type.indexOf("\n")-2);

                        JsonArray CategoryJsonArrayReq = new JsonArray();
                        CategoryJsonArrayReq.add(CategoryRequestJson);

                        SignupJsonRequest.addProperty("title",str_prifixname);
                        SignupJsonRequest.addProperty("fname",str_firstName);
                        SignupJsonRequest.addProperty("gender",str_gender);
                        SignupJsonRequest.addProperty("lname",str_lastName);
                        SignupJsonRequest.addProperty("email",str_email);
                        SignupJsonRequest.addProperty("serialNumber","1");
                        SignupJsonRequest.addProperty("password",str_password);
                        SignupJsonRequest.addProperty("currencySign","gbp");
                        SignupJsonRequest.addProperty("category",CategoryJsonArrayReq.toString());


                        SignupJsonRequest.addProperty("accountant_status","open");
                        SignupJsonRequest.addProperty("package",str_plan);
                        SignupJsonRequest.addProperty("trialPlan",str_plan_type);
                        SignupJsonRequest.addProperty("mobile",str_mobile);
                        SignupJsonRequest.addProperty("role","locum");
                        SignupJsonRequest.addProperty("tc","1");
                        //SignupJsonRequest.addProperty("securityQues",str_password);
                        //SignupJsonRequest.addProperty("securityAns","");
                        SignupJsonRequest.addProperty(str_TitlecatTypeNumber,str_categoryNumber);
                        //SignupJsonRequest.addProperty("dueDates","gbp");
                        //SignupJsonRequest.addProperty("financial_years",CategoryJsonArrayReq.toString());

                        new SignUpLocumAPI().execute();

                    }


                }else {



                }
            }
        });

    }


    private void loadFragment(Fragment fragment) {
// create a FragmentManager
        FragmentManager fm = getFragmentManager();
// create a FragmentTransaction to begin the transaction and replace the Fragment
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
// replace the FrameLayout with new Fragment
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit(); // save the changes
    }

    @Override
    public void onBackPressed() {


        FragmentManager fm = getFragmentManager();
        fm.popBackStack();

        //getFragmentManager().popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        int count = getFragmentManager().getBackStackEntryCount();
        if (count == 0 || step_no==1){
            super.onBackPressed();
        }else{

            //loadFragment(new SignupStepOne());
            step_no = 1;
            btn_submit.setText("Next");
            ll_stepssignup.setBackgroundResource(R.drawable.stepone);
        }
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    public boolean isPasswordValid(String password) {
        boolean isValid = false;
        String PASSWORD_PATTERN = "((?=.*[a-z])(?=.*\\d)(?=.*[A-Z])(?=.*[@#$%!]).{8,15})";

        Pattern pattern = Pattern.compile(PASSWORD_PATTERN);
        Matcher matcher = pattern.matcher(password);

        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }

    String resUserTitles,resCategory;
    JsonObject CategoryJson = new JsonObject();

    private class GetUeserTitleGetCategory extends AsyncTask<Object, Void, String> {

        @Override
        protected void onPreExecute()//execute thaya pela
        {
            super.onPreExecute();
            // Log.d("pre execute", "Executando onPreExecute ingredients");
            //inicia diálogo de progress, mostranto processamento com servidor.
            progressDialog = ProgressDialog.show(SignupLocum.this, "Loading", "Please Wait...", true, false);

        }

        @Override
        protected String doInBackground(Object... parametros) {

            try {

                String responseUSerTitles = post(Const.SERVER_URL_API +"UserTitles", "","get");
                // Log.d("URL ====",Const.SERVER_URL_API+"filter_venues?"+upend);
                resUserTitles=responseUSerTitles;

                String responseCategory = post(Const.SERVER_URL_API +"locum_categorys", "","get");
                // Log.d("URL ====",Const.SERVER_URL_API+"filter_venues?"+upend);
                resCategory=responseCategory;

            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resUserTitles;
        }


        @Override
        protected void onPostExecute(String result) {

            String response_string = "";
            // System.out.println("OnpostExecute----done-------");
            super.onPostExecute(result);


            try{
                 Log.i("RES UsertitlesCategy---", resUserTitles+"\n---------------------\n"+resCategory);
                JsonParser parser = new JsonParser();


                arrayList_prefixName.clear();
                JsonArray rootArrayUserTitle = parser.parse(resUserTitles).getAsJsonArray();
                for(int a=0; a<rootArrayUserTitle.size();a++){
                    String Title = rootArrayUserTitle.get(a).getAsJsonObject().get("title").getAsString();
                    arrayList_prefixName.add(Title);
                    spinnerArrayAdapter.notifyDataSetChanged();

                }

                arrayList_category.clear();
                arrayList_category.add("Select Category");
                rootArrayCategory = parser.parse(resCategory).getAsJsonArray();
                for(int b=0; b<rootArrayCategory.size();b++){
                    String name = rootArrayCategory.get(b).getAsJsonObject().get("name").getAsString();
                    arrayList_category.add(name);
                    spinnerCategoryAdapter.notifyDataSetChanged();

                    CategoryJson.addProperty("_id", rootArrayCategory.get(b).getAsJsonObject().get("_id").getAsString());
                    CategoryJson.addProperty("name", rootArrayCategory.get(b).getAsJsonObject().get("name").getAsString());
                    CategoryJson.addProperty("__v", rootArrayCategory.get(b).getAsJsonObject().get("__v").getAsString());
                }



            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            progressDialog.dismiss();
        }
    }

    String resUserSigup;
    private class SignUpLocumAPI extends AsyncTask<Object, Void, String> {

        @Override
        protected void onPreExecute()//execute thaya pela
        {
            super.onPreExecute();
            // Log.d("pre execute", "Executando onPreExecute ingredients");
            //inicia diálogo de progress, mostranto processamento com servidor.
            progressDialog = ProgressDialog.show(SignupLocum.this, "Loading", "Please Wait...", true, false);

        }

        @Override
        protected String doInBackground(Object... parametros) {

            try {

               // Log.d("data send--",""+SignupJsonRequest.toString());

                String responseUSerTitles = post(Const.SERVER_URL_API +"users", SignupJsonRequest.toString(),"post");
                // Log.d("URL ====",Const.SERVER_URL_API+"filter_venues?"+upend);
                resUserSigup=responseUSerTitles;


            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resUserSigup;
        }


        @Override
        protected void onPostExecute(String result) {

            String response_string = "";
            // System.out.println("OnpostExecute----done-------");
            super.onPostExecute(result);


            try{
                Log.e("RES resUserSigup---", resUserSigup);
                JsonParser parser = new JsonParser();
                JsonObject rootObjsignup = parser.parse(resUserSigup).getAsJsonObject();

                if(rootObjsignup.has("errors")){

                }else{

                    String token = rootObjsignup.get("token").getAsString();
                    CONST_SHAREDPREFERENCES.edit().putString(PREF_USER_TOKEN,token).apply();
                    /*startActivity(new Intent(SignupLocum.this,DashboardLocum.class));
                    finish();*/
                }
            }
            catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            progressDialog.dismiss();
        }
    }

}
